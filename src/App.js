import React, { Component } from 'react';
import { Grid,  Row, Col } from 'react-flexbox-grid';
import CssBaseline from '@material-ui/core/CssBaseline';
//import WeatherLocation from './components/WeatherLocation';
import Paper from '@material-ui/core/Paper';
import AppBar from  '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

import LocationList from './components/LocationList';
import ForecastExtended from './components/ForecastExtended';

import './App.css';

const cities = [
  'Guayaquil,ec',
  'Quito,ec',
  'Cuenca,ec',
  'Caracas,ve',
  'Bogota,co',
  'Madrid,es',
];

class App extends Component {

  constructor() {
    super();
    this.state = { city: null};
  }

  handleSelectedLocation = city => {
    //console.log(`handleSelectionLocation${city}`);
    this.setState({city});
  }

  render() {
    const {city} = this.state;
    return (
      <React.Fragment>
      <CssBaseline />
        <Grid>
          <Row>
            <Col xs={12}>
            <AppBar position="static">
              <Toolbar>
                <IconButton color="inherit" aria-label="Menu">
                  <MenuIcon />
                </IconButton>
                Weather Location
              </Toolbar>
            </AppBar>
            </Col>
          </Row>
          <Row>
            <Col xs={12} md={6}>
              <LocationList cities={cities}
                onSelectedLocation = {this.handleSelectedLocation}>
              </LocationList>
            </Col>
            <Col xs={12} md={6}>
              <Paper zDepth={4}>
                <div className="detail">
                { !city ? <h1>No se seleccionó ciudad</h1> :
                <ForecastExtended city={city}></ForecastExtended> }
                </div>
              </Paper>
            </Col>
          </Row>
        </Grid>
        {/*<div className="App">
          <LocationList cities={cities}
            onSelectedLocation = {this.handleSelectedLocation}
          ></LocationList>
    </div>*/}
      </React.Fragment>
    );
  }
}

export default App;

import React from 'react';
import PropTypes from 'prop-types';
import WeatherLocation from './WeatherLocation';
import './styles.css';


const LocationList = ({cities, onSelectedLocation}) => {
    const handleWeatherLocationClick = city => {
        //console.log("habdleWeatherLocationClick");
        onSelectedLocation(city);
    }
    const strToComponent = cities => (
        cities.map( city => 
            (
                <WeatherLocation 
                    key={city} 
                    city={city} 
                    onWeatherLocationClick = {() => handleWeatherLocationClick(city)} 
                />) )
    );
    return (<div className="locationList">
        {strToComponent(cities)}
    </div>);
};


/*
Con arreglos sin eventos
const strToComponent = cities => (
    cities.map( city => (<WeatherLocation key={city} city={city} />) )
);
const LocationList = ({cities}) => (
    <div>
        {strToComponent(cities)}
    </div>
);
*/


/*const LocationList = () => (
    <div>
        <WeatherLocation city={'Guayaquil,ec'} />
        <WeatherLocation city={'Quito,ec'} />
        <WeatherLocation city={'Bogota,co'} />
    </div>
);*/

Location.propTypes = {
    cities: PropTypes.array.isRequired,
    onSelectedLocation: PropTypes.func,
}

export default LocationList;
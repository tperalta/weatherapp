import React, { Component } from 'react';
import Location from './Location';
import WeahterData from './WeatherData';
import transformWeather from './../../services/transformWeather';
import './styles.css';
import CircularProgress from '@material-ui/core/CircularProgress';
import PropTypes from 'prop-types'; // ES6

//const city = "Guayaquil,ec";
const api_key = '7d01979f6af84d337178ed9bce0f0b66';
const url = 'http://api.openweathermap.org/data/2.5/weather';
//const api_weather = `${url}?q=${city}&appid=${api_key}`;

/*const data1 = {
    temperature: 20,
    weatherState: SUN,
    humidity: 10,
    wind: '10 m/s',
};*/

/*const data2 = {
    temperature: 10,
    weatherState: SNOW,
    humidity: 5,
    wind: '20 m/s',
};*/

/*
const WeatherLocation = () => (
    <div className='weatherLocationCont'>
        <Location city={'Buenos Aires!'}/>
        <WeahterData data = {data}/>
    </div>
);
*/

class WeatherLocation extends Component { 

    constructor({ city }) {
        super();
        this.state ={
            city,
            data: null
        };
    }

    /*getWeatherState = weahter => {
        return SUN;
    }

    getTemp = kelvin => {
        return convert(kelvin).from('K').to('C').toFixed(2);
    }

    getData = weather_data => {
        const { humidity, temp } = weather_data.main;
        const { speed } = weather_data.wind;
        const weatherState = this.getWeatherState(this.weather);
        const temperature = this.getTemp(temp);

        const data = {
            humidity,
            temperature,
            weatherState,
            wind: `${speed} m/s`,
        }
        return data;
    };*/

    /*handleUpdateClick = () => {
        fetch(api_weather).then( data => {
            return data.json();
         }).then( weather_data => {
            //const data = this.getData(weather_data);
            const data = transformWeather(weather_data);
            this.setState({ data }); //data: data como es el mismo nombre se puede poner solo 1 nombre
         });
        /*this.setState({
            city: 'Guayaquil',
            data: data2
        });*/
        
   /* } */

    //Se ejecuta despues del constructor una sola vez
    componentWillMount() {
        const { city }  = this.state;
        const api_weather = `${url}?q=${city}&appid=${api_key}`;
        fetch(api_weather).then( data => {
            return data.json();
         }).then( weather_data => {
            const data = transformWeather(weather_data);
            this.setState({ data }); //data: data como es el mismo nombre se puede poner solo 1 nombre
         });
    }

    //Se ejecuta despues del render una sola vez
    componentDidMount() {
        //console.log("componentDidMount");
    }

    //Se ejecuta al actualizar el DOM antes del render
    componentWillUpdate(){
        //console.log("componentWillUpdate");
    }

    //Se ejecuta al actualizar el DOM despues del render
    componentDidUpdate() {
        //console.log("componentDidUpdate");
    }

    render = () => {
        //console.log("render");
        const {onWeatherLocationClick} = this.props;
        const { city, data } = this.state;

        return(<div className='weatherLocationCont' onClick={onWeatherLocationClick}>
            <Location city={city}/>
            {data ? <WeahterData data = {data}/> : 
            <CircularProgress size={50} thickness={7}/>}
        </div>);

        /*return(<div className='weatherLocationCont'>
            <Location city={city}/>
            <WeahterData data = {data}/>
            <button onClick={this.handleUpdateClick} >Actualizar</button>
        </div>);*/
    };
}

WeatherLocation.propTypes = {
    city: PropTypes.string,
    onWeatherLocationClick: PropTypes.func,
}
export default WeatherLocation;